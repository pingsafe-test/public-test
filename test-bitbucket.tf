resource "google_storage_bucket" "Pingsafe-CloudStorageBucket-Public" {
  name          = "pingsafe-cloudstoragebucket-public"
  location      = "EU"
  force_destroy = true
  website {
    main_page_suffix = "index.html"
    not_found_page   = "404.html"
    }
  cors {
    origin          = ["http://pingsafe.com"]
    method          = ["GET", "HEAD", "PUT", "POST", "DELETE"]
    response_header = ["*"]
    max_age_seconds = 3600
    }
}
resource "google_storage_bucket_iam_binding" "allow_public_read" {
  bucket  = google_storage_bucket.Pingsafe-CloudStorageBucket-Public.id
  members = ["allUsers"]
  role    = "roles/storage.objectViewer"
}

resource "google_storage_bucket" "Pingsafe-CloudStorageBucket-Public" {
  name          = "pingsafe-cloudstoragebucket-public"
  location      = "EU"
  force_destroy = true

}

